import requests
from flask import Flask, request, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/', methods=['POST', 'GET'])
def callAPI():
    nr = request.form['number']
    response = requests.post("https://gyp0g80vvj.execute-api.us-east-2.amazonaws.com/default/myLambda?n="+str(nr))
    if response.status_code == 200:
        output =response._content
        return render_template('index.html', message = output)
    else:
        return response.status_code

if __name__ == '__main__':
  app.run()
